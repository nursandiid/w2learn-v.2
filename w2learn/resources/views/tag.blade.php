@extends('layouts.frontend._posts')

@section('posts')
<!-- Latest Posts -->
    <main class="col-lg-8"> 
      <div class="container">
        <div class="row">
            <div class="col mt-5">
                <h3>#{{ $tag->name }}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mt-4 wow fadeIn">
                    <div class="card post border-0 mb-3">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-format="fluid"
                             data-ad-layout-key="-5m+d1+5s-mf+hy"
                             data-ad-client="ca-pub-9070824081326686"
                             data-ad-slot="5334813540"></ins>
                        <script>
                             (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
        @if($posts->count() > 0)
            @foreach($posts as $post)
                <div class="col-md-6 mt-4 wow fadeIn">
                    <div class="card post border-0">
                        <img src="{{ Storage::disk('public')->url('post/'. $post->image) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <div class="post-meta d-flex justify-content-between">
                                <div class="date meta-last">{{ $post->created_at->toFormattedDateString() }}</div>
                                <div class="category">
                                    @foreach($post->tags as $tag)
                                        <a href="{{ route('tag.posts',$tag->slug) }}">{{ $tag->name }}</a>
                                    @endforeach
                                </div>
                            </div>
                            <h3 class="h4"><a href="{{ route('post.details', $post->slug) }}" class="text-dark">{{ $post->title }}</a></h3></a>
                            <p class="text-muted">
                                @php  
                                    $body = explode(' ', $post->body);
                                    $body = implode(' ', array_splice($body, 0, 20));
                                @endphp

                                {!! $body !!} ...
                            </p>
                            <div class="post-details">
                                <footer class="post-footer d-flex align-items-center"><a href="#" class="author d-flex align-items-center flex-wrap">
                                    <div class="avatar"><img src="{{ Storage::disk('public')->url('profile/'.$post->user->image) }}" alt="..." class="img-fluid"></div>
                                    <div class="title text-uppercase" style="font-size: .8em;"><span>{{ $post->user->name }}</span></div></a>
                                    <div class="date meta-last" style="font-size: .8em;"><i class="fa fa-clock-o"></i> {{ $post->created_at->diffForHumans() }}</div>
                                </footer>
                            </div>
                        </div>
                        <div class="card-footer border-top-0">
                            <ul class="nav justify-content-center">
                                <li class="nav-item text-center" style="width: 33.3%;">
                                    @guest
                                    <a href="javascript:void(0);" class="text-muted" style="font-size: .8em;" onclick="toastr.info('To add favorite list. You need to login first.','Info',{
                                        closeButton: true,
                                        progressBar: true,
                                    })"><i class="fa fa-heart mr-1"></i> {{ $post->favorite_to_users->count() }}</a>
                                    @else
                                    <a href="javascript:void(0);" class="text-muted"  style="font-size: .8em;"onclick="document.getElementById('favorite-form-{{ $post->id }}').submit();"
                                    class="{{ !Auth::user()->favorite_posts->where('pivot.post_id',$post->id)->count()  == 0 ? 'favorite_posts' : ''}}"><i class="fa fa-heart mr-1"></i>{{ $post->favorite_to_users->count() }}</a>
                                    <form id="favorite-form-{{ $post->id }}" method="POST" action="{{ route('post.favorite',$post->id) }}" style="display: none;">
                                        @csrf
                                    </form>
                                    @endguest
                                </li>
                                <li class="nav-item text-center" style="width: 33.3%;">
                                    <a class="text-muted" href="#"><i class="fa fa-comments-o"></i> <span style="font-size: .7em;">{{ $post->comments->count() }}</span></a>
                                </li>
                                <li class="nav-item text-center" style="width: 33.3%;">
                                    <a class="text-muted" href="#"><i class="fa fa-eye"></i> <span style="font-size: .7em;">{{ $post->view_count }}</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
            @else
            <div class="col mt-5">
                <h3>Sorry, No post found :(</h3>
            </div>
                
            @endif
            <div class="col-md-6 mt-4 wow fadeIn">
                <div class="card post border-0 mb-3">
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-format="fluid"
                         data-ad-layout-key="-5m+d1+5s-mf+hy"
                         data-ad-client="ca-pub-9070824081326686"
                         data-ad-slot="5334813540"></ins>
                    <script>
                         (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection