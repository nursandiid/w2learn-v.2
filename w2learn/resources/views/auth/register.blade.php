<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>W2Learn</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="{{ asset('assets/blog/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/blog/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
    @include('css.style')
    
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-9070824081326686",
        enable_page_level_ads: true
      });
    </script>
</head>
<body id="home">
    <nav class="navbar navbar-expand-lg d-none">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center">
            <div class="close-btn"><i class="fa fa-times" style="font-size: 1.2em;"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="{{ route('search_posts_sidebar') }}">
                  <div class="form-group">
                    <input type="text" name="query" id="query" placeholder="What are you looking for?">
                    <button type="submit" class="submit"><i class="fa fa-search"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </nav>
    <header id="header" class="fixed-top">
        <div class="container">
            <div class="logo float-left">
                <a href="#home" class="scrollto"><img src="{{ asset('assets/blog/img/logo.png') }}" alt="" class="img-fluid"></a>
            </div>
            <nav class="main-nav float-right d-none d-lg-block">
                <ul>
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ url('/posts') }}">Blog</a></li>
                    <li><a href="{{ url('/') }}#portfolio1">Template</a></li>
                    <li class="drop-down"><a href="#">Informasi</a>
                        <ul>
                            <li><a href="#">Tentang Kami</a></li>
                            <li><a href="#">Ketentuan</a></li>
                            <li><a href="#">Kebijakan</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ url('/contact') }}">Contact Us</a></li>
                    @guest
                    <li class="drop-down active"><a href="#">Mores</a>
                        <ul>
                          <li><a href="{{ url('/register') }}"><i class="fa fa-terminal"></i> Register</a></li>
                          <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
                        </ul>
                    </li>          
                    @endguest
                    <li><a href="#" class="search-btn d-none text-muted d-lg-block"><i class="fa fa-search"></i></a></li>
                </ul>
            </nav><!-- .main-nav -->
            
        </div>
    </header><!-- #header -->
    
    <main id="main" style="margin-top: 5em;">
        <div class="container">
            <div class="section-header">
                <h3>Register to Write Posts</h3>
            </div>

            <div class="row my-5">
                <div class="col-md-12">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-4">
                                <input id="name" type="text" class="border-primary form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-4">
                                <input id="username" type="text" class="border-primary form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-4">
                                <input id="email" type="email" class="border-primary form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="border-primary form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="border-primary form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-outline-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </main>

    <footer id="footer" style="margin-top: -7.2em;">
        @include('layouts.frontend._footer')
    </footer><!-- #footer -->
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    
    <!-- Uncomment below i you want to use a preloader -->
    <!-- <div id="preloader"></div> -->
    <!-- JavaScript Libraries -->
    <script src="{{ asset('assets/blog/lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/jquery/jquery-migrate.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/mobile-nav/mobile-nav.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/blog/lib/lightbox/js/lightbox.min.js') }}"></script>
    <!-- Contact Form JavaScript File -->
    <script src="{{ asset('assets/blog/contactform/contactform.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/tether.min.js') }}"></script>
    <!-- Template Main Javascript File -->
    <script src="{{ asset('assets/blog/js/main.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/swiper.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/scripts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    {!! Toastr::message() !!}
    <script>
        $('.search-btn').on('click', function (e) {
        e.preventDefault();
        $('.navbar-expand-lg').removeClass('d-none')
        $('.search-area').fadeIn();
        $('#search').focus()
    });
    
    $('.search-area .close-btn').on('click', function () {
        $('.navbar-expand-lg').addClass('d-none')
        $('.search-area').fadeOut();
    });
    
    @if($errors->any())
        @foreach($errors->all() as $error)
            toastr.error('{{ $error }}','Error',{
                closeButton:true,
                progressBar:true,
            });
        @endforeach
    @endif

    </script>
    @stack('js')
</body>
</html>