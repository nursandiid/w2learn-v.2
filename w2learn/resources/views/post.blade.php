@extends('layouts.frontend._posts')

@section('posts')
<!-- Latest Posts -->
<main class="post blog-post col-lg-8 mt-5">
    <div class="container card border-0">
        <div class="post-single">
            <div class="post-thumbnail mt-3">
                <img src="{{ Storage::disk('public')->url('post/'. $post->image) }}" alt="..." class="img-fluid border rounded">
            </div>
            <div class="post-details">
                <div class="post-meta d-flex justify-content-between">
                    <div class="category">
                        @foreach($post->tags as $tag)
                            <a href="{{ route('tag.posts',$tag->slug) }}">{{ $tag->name }}</a>
                        @endforeach
                    </div>
                </div>
                <h1>{{ $post->title }}<a href="#"><i class="fa fa-bookmark-o"></i></a></h1>
                <div class="post-footer d-flex align-items-center flex-column flex-sm-row"><a href="#" class="author d-flex align-items-center flex-wrap">
                    <div class="avatar"><img src="{{ Storage::disk('public')->url('profile/'.$post->user->image) }}" alt="..." class="img-fluid"></div>
                    <div class="title text-uppercase"><span>{{ $post->user->name }}</span></div></a>
                    <div class="d-flex align-items-center flex-wrap">
                        <div class="date"><i class="fa fa-calendar"></i> {{ $post->created_at->diffForHumans() }}</div>
                        <div class="views"><i class="fa fa-eye"></i> {{ $post->view_count }}</div>
                        <div class="comments meta-last"><i class="fa fa-comments-o"></i>{{ $post->comments->count() }}</div>
                    </div>
                </div>
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-format="fluid"
                     data-ad-layout-key="-5m+d1+5s-mf+hy"
                     data-ad-client="ca-pub-9070824081326686"
                     data-ad-slot="5334813540"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
                <div class="post-body">
                    {!! ($post->body) !!}
                </div>

                <div class="post-tags">
                    @foreach($post->tags as $tag)
                        <a href="{{ route('tag.posts', $tag->slug) }}" class="tag">#{{ $tag->name }}</a>
                    @endforeach
                </div>

                <div class="posts-nav d-flex justify-content-between align-items-stretch flex-column flex-md-row">
                    @if ($paginates->count())
                    @for ($i = 0; $i < 1; $i++)
                        <a href="{{ route('post.details', $paginates[0]->slug) }}" class="prev-post text-left d-flex align-items-center">
                            <div class="icon prev"><i class="fa fa-angle-left"></i></div>
                            <div class="text">
                                <strong class="text-primary" style="visibility: hidden;">Previous Post </strong>
                                <h6>{{ $paginates[0]->title }}</h6>
                            </div>
                        </a>
                        <a href="{{ route('post.details', $paginates[1]->slug) }}" class="next-post text-right d-flex align-items-center justify-content-end">
                            <div class="text">
                                <strong class="text-primary" style="visibility: hidden;">Next Post </strong>
                                <h6>{{ $paginates[1]->title }}</h6>
                            </div>
                            <div class="icon next">
                                <i class="fa fa-angle-right">   </i>
                            </div>
                        </a>
                    @endfor
                    @endif
                </div>
                
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- w2learn-display -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-9070824081326686"
                     data-ad-slot="9828947544"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>


                <div class="post-comments">
                        <div id="disqus_thread"></div>
                        <script>
                        (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://https-w2learn-com.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                </div>
                <div class="post-comments d-none d-lg-none">
                    <header>
                        <h3 class="h6">Post Comments<span class="no-of-comments">({{ $post->comments()->count() }})</span></h3>
                    </header>
                    
                    @if($post->comments->count() > 0)
                        @foreach($post->comments as $comment)
                        <div class="comment">
                            <div class="comment-header d-flex justify-content-between">
                                <div class="user d-flex align-items-center">
                                    <div class="image">
                                        <img src="{{ Storage::disk('public')->url('profile/'.$comment->user->image) }}" alt="..." class="img-fluid rounded-circle">
                                    </div>
                                    <div class="title">
                                        <strong>{{ $comment->user->name }}</strong>
                                        <span class="date">{{ $comment->created_at->diffForHumans() }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-body">
                                <p>{{ $comment->comment }}</p>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <p>No Comment yet. Be the first :)</p>
                    @endif
                </div>
                    
                <div class="add-comment d-none d-lg-none">
                    <header>
                        <h3 class="h6">Leave a reply</h3>
                    </header>
                       
                    @guest
                        <p>For post a new comment. You need to login first. <a href="{{ route('login') }}">Login</a></p>
                    @else
                    <form method="post" action="{{ route('comment.store', $post->id) }}" class="commenting-form">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" id="username" placeholder="Name" class="form-control rounded-0">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" id="useremail" placeholder="Email Address (will not be published)" class="form-control rounded-0">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea rows="5" name="comment" id="comment" placeholder="Type your comment .." class="form-control rounded-0"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-primary float-right">Post Comment</button>
                            </div>
                        </div>
                    </form>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
