@extends('layouts.frontend._home')

@section('card-content')

@if($posts->count() > 0)
@php $i = 1;  @endphp

@foreach($posts as $post)
    <div class="col-md-4 mt-5 wow fadeInUp" data-wow-duration="1.<?= $i + 1; ?>s">
        <div class="card post border-0">
            <img src="{{ Storage::disk('public')->url('post/'. $post->image) }}" class="card-img-top" alt="...">
            <div class="card-body">
                <div class="post-meta d-flex justify-content-between">
                    <div class="date meta-last">{{ $post->created_at->toFormattedDateString() }}</div>
                    <div class="category">
                        @foreach($post->tags as $tag)
                            <a href="{{ route('tag.posts',$tag->slug) }}">{{ $tag->name }}</a>
                        @endforeach
                    </div>
                </div>
                <h3 class="h4"><a href="{{ route('post.details', $post->slug) }}" class="text-dark">{{ $post->title }}</a></h3></a>
                <p class="text-muted d-lg-none d-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                <div class="post-details">
                    <footer class="post-footer d-flex align-items-center"><a href="#" class="author d-flex align-items-center flex-wrap">
                        <div class="avatar"><img src="{{ Storage::disk('public')->url('profile/'.$post->user->image) }}" alt="..." class="img-fluid"></div>
                        <div class="title text-uppercase" style="font-size: .8em;"><span>{{ $post->user->name }}</span></div></a>
                        <div class="date meta-last" style="font-size: .8em;"><i class="fa fa-clock-o"></i> {{ $post->created_at->diffForHumans() }}</div>
                    </footer>
                </div>
            </div>
            <div class="card-footer border-top-0">
                <ul class="nav justify-content-center">
                    <li class="nav-item text-center" style="width: 33.3%;">
                        @guest
                        <a href="javascript:void(0);" class="text-muted" style="font-size: .8em;" onclick="toastr.info('To add favorite list. You need to login first.','Info',{
                            closeButton: true,
                            progressBar: true,
                        })"><i class="fa fa-heart mr-1"></i> {{ $post->favorite_to_users->count() }}</a>
                        @else
                        <a href="javascript:void(0);" class="text-muted"  style="font-size: .8em;"onclick="document.getElementById('favorite-form-{{ $post->id }}').submit();"
                        class="{{ !Auth::user()->favorite_posts->where('pivot.post_id',$post->id)->count()  == 0 ? 'favorite_posts' : ''}}"><i class="fa fa-heart mr-1"></i>{{ $post->favorite_to_users->count() }}</a>
                        <form id="favorite-form-{{ $post->id }}" method="POST" action="{{ route('post.favorite',$post->id) }}" style="display: none;">
                            @csrf
                        </form>
                        @endguest
                    </li>
                    <li class="nav-item text-center" style="width: 33.3%;">
                        <a class="text-muted" href="#"><i class="fa fa-comments-o"></i> <span style="font-size: .7em;">{{ $post->comments->count() }}</span></a>
                    </li>
                    <li class="nav-item text-center" style="width: 33.3%;">
                        <a class="text-muted" href="#"><i class="fa fa-eye"></i> <span style="font-size: .7em;">{{ $post->view_count }}</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endforeach
@endif

@endsection

@push('js')
    <script>
        $('#intro').hide()
        $('#articles .section-header h3').html('{{ $posts->count() }} Hasil Pencarian Untuk {{ $query }}')
    </script>
@endpush