<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>W2Learn</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{ asset('assets/blog/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('assets/blog/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  @include('css.style')
  
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-9070824081326686",
        enable_page_level_ads: true
      });
    </script>
</head>
<body id="home">
  <nav class="navbar navbar-expand-lg d-none">
    <div class="search-area">
      <div class="search-area-inner d-flex align-items-center justify-content-center">
        <div class="close-btn"><i class="fa fa-times" style="font-size: 1.2em;"></i></div>
        <div class="row d-flex justify-content-center">
          <div class="col-md-8">
            <form action="{{ route('search_posts_sidebar') }}">
              <div class="form-group">
                <input type="text" name="query" id="query" placeholder="What are you looking for?">
                <button type="submit" class="submit"><i class="fa fa-search"></i></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </nav>
  <header id="header" class="fixed-top">
    <div class="container">
      <div class="logo float-left"> 
        <a href="#home" class="scrollto"><img src="{{ asset('assets/blog/img/logo.png') }}" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{ url('/posts') }}">Blog</a></li>
          <li><a href="{{ url('/') }}#portfolio1">Template</a></li>
          <li class="drop-down"><a href="#">Informasi</a>
            <ul>
              <li><a href="#">Tentang Kami</a></li>
              <li><a href="#">Ketentuan</a></li>
              <li><a href="#">Kebijakan</a></li>
            </ul>
          </li>
          <li class="active"><a href="{{ url('/contact') }}">Contact Us</a></li>
          @guest
          <li class="drop-down"><a href="#">Mores</a>
            <ul>
              <li><a href="{{ url('/register') }}"><i class="fa fa-terminal"></i> Register</a></li>
              <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
            </ul>
          </li>          
          @endguest
          <li><a href="#" class="search-btn d-none text-muted d-lg-block"><i class="fa fa-search"></i></a></li>
          </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  <main id="main">

    <section id="contact">
      <div class="container-fluid">

        <div class="section-header mt-3">
          <h3>Contact Us</h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">
              <iframe src="https://maps.google.com/maps?q=cirebon&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p>Cirebon City, West Java</p>
              </div>
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p>support@w2learn.com</p>
              </div>
              <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p>+62 813 2477 9934</p>
              </div>
            </div>

            <div class="form">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="" method="post" role="form" class="contactForm">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-right"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

  </main>

  <footer id="footer" style="margin-top: -7.2em;">
    @include('layouts.frontend._footer')
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="{{ asset('assets/blog/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/mobile-nav/mobile-nav.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/wow/wow.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/waypoints/waypoints.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/isotope/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/lightbox/js/lightbox.min.js') }}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('assets/blog/contactform/contactform.js') }}"></script>
  <script src="{{ asset('assets/frontend/js/tether.min.js') }}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('assets/blog/js/main.js') }}"></script>
  <script src="{{ asset('assets/frontend/js/swiper.js') }}"></script>
  <script src="{{ asset('assets/frontend/js/scripts.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  {!! Toastr::message() !!}
  <script>

    $('.search-btn').on('click', function (e) {
        e.preventDefault();
        $('.navbar-expand-lg').removeClass('d-none')
        $('.search-area').fadeIn();
        $('#search').focus()
    });
    $('.search-area .close-btn').on('click', function () {
        $('.navbar-expand-lg').addClass('d-none')
        $('.search-area').fadeOut();
    });

      @if($errors->any())
      @foreach($errors->all() as $error)
      toastr.error('{{ $error }}','Error',{
          closeButton:true,
          progressBar:true,
      });
      @endforeach
      @endif
  </script>
  @stack('js')

</body>
</html>
